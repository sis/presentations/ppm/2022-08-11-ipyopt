# ipyopt - Interior Point Optimizing with Python

This repository contains the slides for the project presentation meeting talk.

## Generating the slides

The [reveal.js](https://revealjs.com/) slides can be generated from
the Markdown file with [pandoc](https://pandoc.org/) using the
following command:

```bash
pandoc \
  -o index.html \
  --standalone \
  -t revealjs \
  --slide-level=2 \
  --shift-heading-level-by=1 \
  --mathjax \
  -V theme=black \
  -V transition=none \
  -V revealjs-url=https://unpkg.com/reveal.js@4.2.1 \
  -H header.html \
  index.md
```

Note that the package `libjs-mathjax` (Ubuntu Focal) is required to render the math in the slides correctly.

Live version:
[siscourses.ethz.ch/ppm/2022-08-11-ipyopt](https://siscourses.ethz.ch/ppm/2022-08-11-ipyopt)
